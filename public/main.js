//window.onload = function() {

//https://github.com/MaiaVictor/lambda-calculus
lam=function(){function Lam(a){return{ctor:"Lam",body:a}}function App(a,b){return{ctor:"App",func:a,argm:b}}function Var(a){return{ctor:"Var",index:a}}function fold(a,b,c){return function d(e){switch(e.ctor){case"Var":return a(e.index);case"Lam":return b(d(e.body));case"App":return c(d(e.func),d(e.argm));}}}function foldScoped(a,b,c){return function(d){return fold(function(b){return function(c){return a(c-1-b)}},function(a){return function(c){return b(c,a(c+1))}},function(a,b){return function(e){return c(a(e),b(e))}})(d)(0)}}function toName(a){var b="";do b+="abcdefghijklmnopqrstuvwxyz"[a%26],a=Math.floor(a/26);while(0<a);return b}function transmogrify(a,b){return foldScoped(function(a){return toName(a)},function(b,c){return a(toName(b),c)},function(a,c){return b(a,c)})}function fromNumber(a){return Lam(Lam(function a(b){return 0===b?Var(0):App(Var(1),a(b-1))}(a)))}function toNumber(a){return toFunction(a)(function(a){return a+1})(0)}function fromFunction(a){return function a(b){return function(c){function d(b){function c(c){return null===c?b:d(function(d){return App(b(d),a(c)(d))})}return c.isApp=!0,c}if(b.isApp)return b(null)(c);if("function"==typeof b){var e=a(b(d(function(a){return Var(a-1-c)})))(c+1);return Lam(e)}return b}}(a)(0)}function toFunction(term){return eval(transmogrify(function(a,b){return"(function("+a+"){return "+b+"})"},function(a,b){return a+"("+b+")"})(term))}function fromString(a){var b=0;return function c(d,e,f){for(;/[^a-zA-Z0-9\(_]/.test(a[b]);)++b;if("("===a[b]){++b;for(var g=c(d,e,f);")"!==a[b];)g=App(g,c(d,e,f));return++b,g}for(var h="";/[a-zA-Z0-9_]/.test(a[b])&&b!==a.length;)h+=a[b++];switch(a[b]){case".":return Lam(c(d+1,e.concat(h),f.concat(null)));case":":var i=c(d,e,f),j=c(d+1,e.concat(h),f.concat(i));return j;default:var k=e.lastIndexOf(h);return f[k]||Var(d-k-1);}}(0,[],[])}function fromBLC(a){var b=0;return function c(){if("0"===a[b])return"0"===a[b+1]?(b+=2,Lam(c())):(b+=2,App(c(),c()));for(var d=0;"0"!==a[b];++d)++b;return++b,Var(d-1)}()}function fromBLC64(a){for(var b,c="",e=0,f=a.length;e<f;++e)b=base64Table[a[e]],c+=0==e?b.slice(b.indexOf("1")+1):b;return fromBLC(c)}function toBLC64(a){for(var b=toBLC(a),c="",e=0,f=b.length;e<=f/6;++e){var g=f-6*e-6,h=0>g?("000001"+b.slice(0,g+6)).slice(-6):b.slice(g,g+6);c=base64Table[h]+c}return c}function reduce(a){return fromFunction(toFunction(a))}var alphabet="abcdefghijklmnopqrstuvwxyz";var fromBruijn=function(a){var b=0;return function c(){if("L"===a[b])return++b,Lam(c());if("("===a[b]){++b;var d=c();++b;var e=c();return++b,App(d,e)}for(var f=0;/[0-9]/.test(a[b]);)f=10*f+ +a[b],++b;return Var(f)}()},toBruijn=fold(function(a){return a},function(a){return"L"+a},function(a,b){return"("+a+" "+b+")"});var toString=transmogrify(function(a,b){return a+"."+b},function(a,b){return"("+a+" "+b+")"});for(var toBLC=fold(function(a){for(var b=0,c="";b<=a;++b)c+="1";return c+"0"},function(a){return"00"+a},function(c,a){return"01"+c+a}),base64Table={},i=0;64>i;++i){var bin=("000000"+i.toString(2)).slice(-6),b64="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i];base64Table[bin]=b64,base64Table[b64]=bin}return{Lam:Lam,App:App,Var:Var,fold:fold,foldScoped:foldScoped,fromNumber:fromNumber,toNumber:toNumber,fromFunction:fromFunction,toFunction:toFunction,fromBruijn:fromBruijn,toBruijn:toBruijn,fromString:fromString,toString:toString,toBLC:toBLC,fromBLC:fromBLC,toBLC64:toBLC64,fromBLC64:fromBLC64,reduce:reduce}}();

if(location.hash) {
    var code = lam.toString(lam.fromBLC64(location.hash.substr(1)))
    var input = parseInt(prompt('Input Number'))
    var beforeCode = '(('
    var afterCode = ')(f.x.('
    var afterAfterCode = 'x)))'
    while(input) {
        afterCode += 'f('
        afterAfterCode += ')'
        input--
    }
    code = beforeCode + code + afterCode + afterAfterCode
    var result = lam.toBruijn(lam.reduce(lam.fromString(code)))
    alert(result.split('1').length - 1)
} else {
    prompt('Copy This', 'https://blc64.now.sh/#'+lam.toBLC64(lam.fromString(prompt('Enter Lambda Calculus Here'))))
}

//}